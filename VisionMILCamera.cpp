#include <StdAfx.h>
#include "VisionHeader.h"
#include "VisionMILCamera.h"

static HookDataStruct	UserHookData;
long MFTYPE ProcessingFunction(long HookType, MIL_ID HookId, void MPTYPE *HookDataPtr);

CVisionMILCamera::CVisionMILCamera(const MIL_ID milApplication, const MIL_ID milSystem,
								   const double dbDispXZoom/*=1*/, const double dbDispYZoom/*=1*/,
								   const double dbGrabXScale/*=1*/, const double dbGrabYScale/*=1*/)
{
	m_milApplication = milApplication;
	m_milSystem = milSystem;
	m_milDigtizer = M_NULL;
	m_milDisplay = M_NULL;
	m_milRawImage = M_NULL;
	m_milShowImage = M_NULL;

	m_dbDispXZoom = dbDispXZoom;
	m_dbDispYZoom = dbDispYZoom;
	m_dbGrabXScale = dbGrabXScale;
	m_dbGrabYScale = dbGrabYScale;

	m_bufSizeX = 0;
	m_bufSizeY = 0;
	m_bufSizeBand = M_NULL;
	m_bufType = M_NULL;
	m_bufLocation = M_NULL;

	m_bIsOpen = FALSE;
	m_bIsContinue = FALSE;
	m_bIsRecord = FALSE;

	m_MilGrabBufferListSize = 0;
}

CVisionMILCamera::~CVisionMILCamera()
{
	//如果已经打开过了则关闭
	if (m_bIsOpen == TRUE)
	{
		Close();
	}
}

//打开相机，分配对应的内存
BOOL CVisionMILCamera::Open()
{
	//如果已经打开过了则先关闭
	if (m_bIsOpen == TRUE)
	{
		Close();
	}

	if (MsysInquire(m_milSystem, M_DIGITIZER_NUM, M_NULL) > 0)//查询是够存在相机对象
	{
		//分配采集器和显示
		MdigAlloc(m_milSystem, M_DEFAULT, "M_DEFAULT", M_DEFAULT, &m_milDigtizer);
		MdispAlloc(m_milSystem, M_DEFAULT, "M_DEFAULT", M_WINDOWED, &m_milDisplay);

		//设置采集参数
		MdigControl(m_milDigtizer, M_GRAB_SCALE_X, m_dbGrabXScale);
		MdigControl(m_milDigtizer, M_GRAB_SCALE_Y, m_dbGrabYScale);

		//查询得到相机参数
		m_bufSizeX = MdigInquire(m_milDigtizer, M_SIZE_X, M_NULL);
		m_bufSizeY = MdigInquire(m_milDigtizer, M_SIZE_Y, M_NULL);
		MdigInquire(m_milDigtizer, M_SIZE_BAND, &m_bufSizeBand);
		MdigInquire(m_milDigtizer, M_TYPE, &m_bufType);
		if (MsysInquire(m_milSystem, M_SYSTEM_TYPE, M_NULL) == M_SYSTEM_HELIOS_TYPE)
		{
			m_bufLocation = M_ON_BOARD;
		}

		//分配采集原始Buffer和显示Buffer
		MbufAllocColor(m_milSystem, m_bufSizeBand, m_bufSizeX, m_bufSizeY, m_bufType,
					   M_IMAGE+M_PROC+M_GRAB+m_bufLocation, &m_milRawImage);
		MbufAllocColor(m_milSystem, m_bufSizeBand, m_bufSizeX, m_bufSizeY, m_bufType,
					   M_IMAGE+M_DISP+M_GRAB+m_bufLocation, &m_milShowImage);

		//分配录制采集时的Buffer list
		if (m_milDigtizer)
		{
			MappControl(M_ERROR, M_PRINT_DISABLE);
			
			//初始化buffer lists
			long m;
			long LastAllocatedM;
			for(m = 0; m < BUFFERING_SIZE_MAX; m++)
			{
				m_MilGrabBufferList[m] = M_NULL;
			}
			
			//分配尽可能多的buffer lists
			for(m = 0; m < BUFFERING_SIZE_MAX; m++)
			{
				MbufAllocColor(m_milSystem, m_bufSizeBand, m_bufSizeX, m_bufSizeY, m_bufType,
							   M_IMAGE+M_PROC+M_GRAB+m_bufLocation, &m_MilGrabBufferList[m]);
				
				if (m_MilGrabBufferList[m])
				{
					MbufClear(m_MilGrabBufferList[m], 0x0);
					
					LastAllocatedM = m;
					m_MilGrabBufferListSize++;
				}
				else
				{
					break;
				}
				
			}
			MappControl(M_ERROR, M_PRINT_ENABLE);
			
			//防止内存被占完，释放最后一个List Node
			MbufFree(m_MilGrabBufferList[LastAllocatedM]);
			m_MilGrabBufferList[LastAllocatedM] = M_NULL;
			m_MilGrabBufferListSize--;
		}

		return m_bIsOpen=TRUE;
	}
	else
	{
		return m_bIsOpen=FALSE;
	}
}

//关闭相机，释放相应内存
void CVisionMILCamera::Close()
{
	if (m_bIsOpen == TRUE)
	{
		FreeImageForShow();

		if (TRUE == m_bIsContinue)
		{
			StopContonueGrab();
		}
		if (TRUE == m_bIsRecord)
		{
			StopRecordGrab();
		}

		//清除Buffer list
		int m = 0;
		for (m = 0; m < m_MilGrabBufferListSize; m++)
		{
			if(M_NULL != m_MilGrabBufferList[m])
			{
				MbufFree(m_MilGrabBufferList[m]);
				m_MilGrabBufferList[m] = M_NULL;
			}
		}
		if (M_NULL != m_milRawImage)
		{
			MbufFree(m_milRawImage);
			m_milRawImage = M_NULL;
		}
		if (M_NULL != m_milShowImage)
		{
			MbufFree(m_milShowImage);
			m_milShowImage = M_NULL;
		}
		if (M_NULL != m_milDigtizer)
		{
			MdigFree(m_milDigtizer);
		}

		if (M_NULL != m_milDisplay)
		{
			MbufFree(m_milDisplay);
		}

		m_bIsOpen = FALSE;
	}
}

void CVisionMILCamera::AllocImageForShow(const HWND hwnd )
{
	MdispZoom(m_milDisplay, m_dbDispXZoom, m_dbDispYZoom);
	MbufClear(m_milShowImage, 0x0);
	MdispControl(m_milDisplay, M_OVERLAY_CLEAR, M_DEFAULT);
	MdispSelectWindow(m_milDisplay, m_milShowImage, hwnd);
}

void CVisionMILCamera::FreeImageForShow()
{
	MbufClear(m_milShowImage, 0x0);
	MdispSelectWindow(m_milDisplay, NULL, NULL); 
}

void CVisionMILCamera::SingleGrab()
{
	if (TRUE == m_bIsContinue)
	{
		StopContonueGrab();
	}
	if (TRUE == m_bIsRecord)
	{
		StopRecordGrab();
	}
	MdigGrab(m_milDigtizer, m_milRawImage);
	MbufCopy(m_milRawImage, m_milShowImage);
}

void CVisionMILCamera::StartContinueGrab()
{
	if (TRUE == m_bIsRecord)
	{
		StopRecordGrab();
	}

	MdigGrabContinuous(m_milDigtizer, m_milShowImage);
	m_bIsContinue = TRUE;
}

void CVisionMILCamera::StopContonueGrab()
{
	MdigHalt(m_milDigtizer);
	m_bIsContinue = FALSE;
}

void CVisionMILCamera::StartRecordGrab( LPCSTR szFileName )
{
	if (TRUE == m_bIsContinue)
	{
		StopContonueGrab();
	}

	lstrcpy(m_szRecordFile, szFileName);

	//打开文件
	MbufExportSequence(m_szRecordFile, M_AVI_DIB, M_NULL, M_NULL, M_NULL, M_OPEN);

	UserHookData.MilImageDisp        = m_milShowImage;
	UserHookData.ProcessedImageCount = 0;
	UserHookData.MilDigtizer		 = m_milDigtizer;
	lstrcpy(UserHookData.szRecordFile, m_szRecordFile);
	MdigProcess(m_milDigtizer, m_MilGrabBufferList, m_MilGrabBufferListSize,
				M_START, M_DEFAULT, ProcessingFunction, &UserHookData); 
	m_bIsRecord = TRUE;
}

void CVisionMILCamera::StopRecordGrab()
{
	MdigProcess(m_milDigtizer, m_MilGrabBufferList, m_MilGrabBufferListSize,
				M_STOP, M_DEFAULT, ProcessingFunction, &UserHookData); 

	//关闭相应录制文件
	MbufExportSequence(m_szRecordFile, M_AVI_DIB, M_NULL, M_NULL, M_NULL, M_CLOSE);

	m_bIsRecord = FALSE;
}

MIL_ID CVisionMILCamera::GetCaptureImageID()
{
	return m_milRawImage;
}

/**************回调处理函数***************************/
long MFTYPE ProcessingFunction( long HookType, MIL_ID HookId, void MPTYPE *HookDataPtr )
{
	HookDataStruct *UserHookDataPtr = (HookDataStruct *)HookDataPtr;
	MIL_ID ModifiedBufferId;
	char   Text[20]= {'\0',};
	
	/* Retrieve the MIL_ID of the grabbed buffer. */
	MdigGetHookInfo(HookId, M_MODIFIED_BUFFER+M_BUFFER_ID, &ModifiedBufferId);   
	
	/* Increase the frame count. */
	UserHookDataPtr->ProcessedImageCount++;
	
	/* Draw the frame count (if enabled). */
	MOs_ltoa(UserHookDataPtr->ProcessedImageCount, Text, 10);
	MgraText(M_DEFAULT, ModifiedBufferId, 20, 20, Text);
	
	//导出帧到视频
	double ProcessFrameRate;
	MdigInquire(UserHookDataPtr->MilDigtizer, M_PROCESS_FRAME_RATE,   &ProcessFrameRate);
	MbufExportSequence(UserHookDataPtr->szRecordFile, M_AVI_DIB, &ModifiedBufferId, 1, 
					   ProcessFrameRate, M_WRITE);
	
	/* Perform the processing and update the display. */
	MbufCopy(ModifiedBufferId, UserHookDataPtr->MilImageDisp);
	
	return 0;
}
